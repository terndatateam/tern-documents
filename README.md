# Objective #

The objective of this repository is to host all policy documents related to TERN Data Services and Analytics platform. The sub-folder `latest` contains the newest document.

### What this repository contains? ###
* TERN Terms of Use
* TERN Data Provider Deed
* TERN Data Management Plan Guidelines - Observing Platform 
* TERN Data Licensing Policy and Data Licensing Guidelines
* TERN Data User manuals
* TERN Data Management Plan (draft)
* TERN Data Management Policy (draft)
* TERN Data Management Framework (draft)


### Help? ###

Please email esupport(at)tern.org.au for any queries
